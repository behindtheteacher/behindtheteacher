package com.college.exception;

public class TeacherNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TeacherNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TeacherNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
