package com.college.exception;

public class TeacherNotAddException extends TeacherNotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TeacherNotAddException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TeacherNotAddException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
