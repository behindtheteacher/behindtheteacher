package com.college.exception;

public class NoTeacherAvailableException extends TeacherNotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoTeacherAvailableException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NoTeacherAvailableException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
