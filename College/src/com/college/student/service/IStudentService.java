package com.college.student.service;

import java.util.HashMap;
import java.util.List;

import com.college.exception.ScheduleNotFoundException;
import com.college.exception.StudentNotFoundException;
import com.college.exception.TeacherNotFoundException;
import com.college.student.model.Student;
import com.college.teacher.model.Teacher;

public interface IStudentService {
List<Student> showStudentsDetails() throws StudentNotFoundException, Exception;
void addStudent(Student stud) throws Exception ;
HashMap<String, String>showSchedule(String sectionName) throws  ScheduleNotFoundException,Exception;
int applyLeave(String name) throws Exception;
int showLeave() throws Exception;
}
