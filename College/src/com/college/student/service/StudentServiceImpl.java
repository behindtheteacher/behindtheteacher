package com.college.student.service;

import java.util.HashMap;
import java.util.List;

import com.college.exception.ScheduleNotFoundException;
import com.college.exception.StudentNotFoundException;
import com.college.lecture.model.Lecture;
import com.college.student.model.Student;
import com.college.student.repo.IStudentRepository;
import com.college.student.repo.StudentRepository;

public class StudentServiceImpl implements IStudentService {
IStudentRepository studentRepository=new StudentRepository();

@Override
public List<Student> showStudentsDetails() throws Exception {
	List<Student> studentDetails=studentRepository.showStudentsDetails();
	if(studentDetails.isEmpty()) {
		
		
		throw new StudentNotFoundException("Student database is not available");
	}
	return studentDetails;
}
public void addStudent(Student student) throws Exception{
	studentRepository.addStudent(student);
}
@Override
public HashMap<String, String> showSchedule(String sectionName) throws ScheduleNotFoundException,Exception {
	HashMap<String, String> showDetails  = new HashMap<String, String>();
	List<Lecture> showSchedules=studentRepository.showSchedule(sectionName);
	for(Lecture lecture: showSchedules) {
		String teacherName= lecture.getTeacherName();
		String firstLec=lecture.getFirstLec();
		String secondLec=lecture.getSecondLec();
		String thridLec=lecture.getThirdLec();
		if(firstLec.equals(sectionName))
			showDetails.put(teacherName,"First Lecture");
		else if(secondLec.equals(sectionName))
			showDetails.put(teacherName,"Second Lecture");
		else if(thridLec.equals(sectionName))
			showDetails.put(teacherName,"Third Lecture");
	}	
if(showSchedules.isEmpty()) {
	throw new ScheduleNotFoundException("Enter correct class Name/ No Schedule Found");
	}
	return showDetails;
}
@Override
public int applyLeave(String name) throws Exception {
	int checkLeave= studentRepository.applyLeave(name);
	if(checkLeave==0) {
		throw new Exception(" pls enter correct Name!!!");
	}
	return checkLeave;
	
}
@Override
public int showLeave() throws Exception {
	int leaveCount= studentRepository.showLeave();
	if(leaveCount>5) {
		throw new Exception("Maximum std apply for leave !!! cant apply");
	}
	return leaveCount;
}
}
