package com.college.student.model;

public class Student {
	String stdName;
	int stdId;
	int year;
	String branch;
	String type;
	String section;
	int leaves;
	public Student() {
		super();
	}
	public Student(String stdName, int stdId, int year, String branch, String type, String section, int leaves) {
		super();
		this.stdName = stdName;
		this.stdId = stdId;
		this.year = year;
		this.branch = branch;
		this.type = type;
		this.section = section;
		this.leaves = leaves;
	}
	public String getStdName() {
		return stdName;
	}
	public void setStdName(String stdName) {
		this.stdName = stdName;
	}
	public int getStdId() {
		return stdId;
	}
	public void setStdId(int stdId) {
		this.stdId = stdId;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public int getLeaves() {
		return leaves;
	}
	public void setLeaves(int leaves) {
		this.leaves = leaves;
	}
	@Override
	public String toString() {
		return "Student [stdName=" + stdName + ", stdId=" + stdId + ", year=" + year + ", branch=" + branch + ", type="
				+ type + ", section=" + section + ", leaves=" + leaves + "]";
	}
}
