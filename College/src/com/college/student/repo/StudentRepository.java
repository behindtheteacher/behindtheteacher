package com.college.student.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.college.dao.ModelDao;
import com.college.lecture.model.Lecture;
import com.college.student.model.Student;

public class StudentRepository implements IStudentRepository {

	@Override
	public List<Student> showStudentsDetails() throws  Exception{
		List<Student> showAll = new ArrayList<>();
		Connection connection = ModelDao.openConnection();
		PreparedStatement ps = null;

		try {

			String showDetails = "Select * from student ";
			ps = connection.prepareStatement(showDetails);
			ResultSet resultset = ps.executeQuery();
			while (resultset.next()){
					Student student = new Student();
					student.setStdName(resultset.getString(1));
					student.setStdId(resultset.getInt(2));
					student.setYear(resultset.getInt(3));
					student.setBranch(resultset.getString(4));
					student.setType(resultset.getString(5));
					student.setSection(resultset.getString(6));
					showAll.add(student);
				}
		} catch (SQLException e) {
		    e.printStackTrace();
			throw e;
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ModelDao.closeConnection();
		}
		return showAll;
	}
	public void addStudent(Student student) throws Exception{
		Connection connection = ModelDao.openConnection();
		PreparedStatement ps = null;
		try {
			String addSql="insert into Student(stdName,stdId,year,branch,type,section,leaves) values(?,?,?,?,?,?,?)";
			ps = connection.prepareStatement(addSql);
			ps.setString(1,student.getStdName());
			ps.setInt(2,student.getStdId());
			ps.setInt(3,student.getYear());
			ps.setString(4,student.getBranch());
			ps.setString(5,student.getType());
			ps.setString(6,student.getSection());
			ps.setInt(7,0);
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ModelDao.closeConnection();
		}

		
	}
	@Override
	public List<Lecture> showSchedule(String sectionName) throws Exception {
		List<Lecture> showSchedule = new ArrayList<>();
		Connection connection = ModelDao.openConnection();
		PreparedStatement ps = null;

		try {
			String showSql="select * from lecture where firstLec=? or secondLec=? or thirdLec=?";
			ps = connection.prepareStatement(showSql);
			ps.setString(1,sectionName);
			ps.setString(2,sectionName);
			ps.setString(3,sectionName);
			ResultSet resultset = ps.executeQuery();
			while (resultset.next()){
					Lecture lecture=new Lecture();
					lecture.setTeacherId(resultset.getInt(1));
					lecture.setFirstLec(resultset.getString(2));
					lecture.setSecondLec(resultset.getString(3));
					lecture.setThirdLec(resultset.getString(4));
					lecture.setTeacherName(resultset.getString(5));
					showSchedule.add(lecture);
				}
		} catch (SQLException e) {
		    e.printStackTrace();
			throw e;
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ModelDao.closeConnection();
		}
		//System.out.println(showSchedule);
		return showSchedule;
	}
	@Override
	public int applyLeave(String name) throws Exception {
			int result = 0;
			Connection connection=null;
			PreparedStatement statement=null;
			try {

				String updsql = "update student set leaves=1 where stdName=?";
				 connection = ModelDao.openConnection();
				 statement = connection.prepareStatement(updsql);
				 statement.setString(1, name.toUpperCase());
				 result = statement.executeUpdate();

			} catch (SQLException e) {

				e.printStackTrace();
				throw e;
			} finally {
				try {

					if (statement != null) {
						statement.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();

				}
				ModelDao.closeConnection();
			}

			return result;
		}
	@Override
	public int showLeave() throws Exception {
		int result = 0;
		Connection connection=null;
		PreparedStatement statement=null;
		try {
			
			String countsql = "select count(*) from student where leaves=1";
			connection = ModelDao.openConnection();
			statement = connection.prepareStatement(countsql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				result = rs.getInt(1);
			}

		} catch (SQLException e) {

			e.printStackTrace();
			throw e;
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();

			}
			ModelDao.closeConnection();
		}

		return result;
	}

}
