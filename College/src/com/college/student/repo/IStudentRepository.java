package com.college.student.repo;
import java.util.List;

import com.college.lecture.model.Lecture;
import com.college.student.model.Student;

public interface IStudentRepository {
	List<Student> showStudentsDetails() throws Exception;
	void addStudent(Student student) throws Exception;
	List<Lecture> showSchedule(String sectionName) throws Exception;
	int applyLeave(String name) throws Exception;
	int showLeave() throws Exception;
}
