package com.college.client;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import com.college.exception.NoTeacherAvailableException;
import com.college.exception.ScheduleNotFoundException;
import com.college.exception.StudentNotFoundException;
import com.college.exception.TeacherNotAddException;
import com.college.exception.TeacherNotFoundException;
import com.college.lecture.model.Lecture;
import com.college.principal.service.IPrincipalService;
import com.college.principal.service.PrincipalServiceImpl;
import com.college.student.model.Student;
import com.college.student.service.IStudentService;
import com.college.student.service.StudentServiceImpl;
import com.college.teacher.model.Teacher;
import com.college.teacher.service.ITeacherService;
import com.college.teacher.service.TeacherServiceImpl;


public class College {

	public static void main(String[] args) throws Exception {
		IPrincipalService PrincipalServiceImpl= new PrincipalServiceImpl();
		ITeacherService teacherRef=new TeacherServiceImpl();
		IStudentService studentRef=new StudentServiceImpl();
		Scanner kb= new Scanner(System.in);
		//int type=kb.nextInt();
			int type=0;
		Scanner s=new Scanner(System.in);
		//Scanner s = new Scanner(System.in);
		//int type=0;
		do {
			System.out.println("--------Select the type---------");
			System.out.println(" 1.Student\n 2.Teacher\n 3.Principal");
			  type= s.nextInt();
		
		
			 
		switch(type) {
		
		case 1:
			int m=0;
			do {
				System.out.println("--------Select the type---------");
				System.out.println(" 1. show Student Details \n 2.Add Student\n 3.show schedule \n 4. Apply for a Leave");
				m=s.nextInt();
			switch(m) {
			case 1:
			    System.out.println("Student Details");
			try {
				
				studentRef.showStudentsDetails().forEach((student)->{
					System.out.print(" std Name -- "+student.getStdName());
					System.out.print(" ,std Id -- "+student.getStdId());
					System.out.print(" ,Year -- "+student.getYear());
					System.out.print(" , Branch -- "+student.getBranch());
					System.out.print(" , Section -- "+student.getSection());
					System.out.println();
					
				});
			} catch (StudentNotFoundException e1) {
				e1.printStackTrace();
			} catch (Exception e1) {
				e1.printStackTrace();
				
			}
			break;
		case 2:
			System.out.println("Enter the name");
			String name=s.next();
			System.out.println("Enter the id");
			int id=s.nextInt();
			System.out.println("Enter the year");
			int year=s.nextInt();
			System.out.println("Enter the branch");
			String branch=s.next();
			System.out.println("Enter the section");
			String section=s.next();
			Student stud=new Student();
			stud.setStdName(name);
			stud.setStdId(id);
			stud.setYear(year);
			stud.setBranch(branch);
			stud.setType("Student");
			stud.setSection(section);
			try {
				studentRef.addStudent(stud);
				System.out.println("student Added");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			break;
			
		case 3:
			System.out.println("Enter the section name:");
				s.nextLine();
			String secname=s.nextLine();
			try {
				HashMap<String, String> getDetails =studentRef.showSchedule(secname);
				System.out.println(getDetails);
			} catch (ScheduleNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			break;
		case 4:
			try {
			System.out.println("enter name");
			String leave=kb.nextLine();
			studentRef.showLeave();
			int checkLeave= studentRef.applyLeave(leave);
			if(checkLeave!=0) {
				System.out.println("applied for leave!! Thank you..");
			}
			}
			catch(Exception e) {
				System.out.println(e.getMessage());
			}
			break;
			}
		
	}while(m!=0);
			
		
		
		
		case 2:
			int n=0;
			do {
			System.out.println("-----------Select the Method to Search----------");
			System.out.println(
					"Search by:\n 1.Department Name\n 2.Faculty Name\n 3.View Schedule\n 4.Apply Leave\n 5.Grant Leave to student \n 6. Press 0 to exit");
			 n= s.nextInt();

			switch (n) {
			case 1: {
				System.out.println("Enter Department Name");
				s.nextLine();
				String dept = s.nextLine();
				try {
					List<Teacher> teacherByDept = teacherRef.searchByDepartment(dept);
					teacherByDept.forEach(teacher->{
						System.out.println("Teacher Name :" + teacher.getTeacherName());
						System.out.println("Department:" + teacher.getDepartment());
						System.out.println("Subject: " + teacher.getSubject());
						System.out.println();
					});
				} catch (TeacherNotFoundException e) {
					System.out.println(e.getMessage());
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			}

			case 2:
				System.out.println("Enter Faculty Name");
				s.nextLine();
				String name = s.nextLine();
				try {
					Teacher teacher = teacherRef.searchByName(name);

					System.out.println("Teacher Name :" + teacher.getTeacherName());
					System.out.println("Department: " + teacher.getDepartment());
					System.out.println("Subject: " + teacher.getSubject());
					System.out.println();
				} catch (TeacherNotFoundException e) {
					System.out.println(e.getMessage());
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;

			case 3:

				System.out.println("Enter Faculty Name to see the Schedule");
				s.nextLine();
				String fname = s.nextLine();
				try {
					List<String> lectures= PrincipalServiceImpl.showTeacherLecture(fname);
					for(int i=0;i<lectures.size();i++) {
						System.out.println((i+1)+" lectures "+lectures.get(i));
						}
				} catch (TeacherNotFoundException e) {
					System.out.println(e.getMessage());
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

				break;

			case 4:

				System.out.println("Enter Name");
				s.nextLine();
				
				String fname1 = s.nextLine();
				try {
					int leave = teacherRef.applyLeave(fname1);
					if(leave!=0) {
						System.out.println("You applied for Leave!! it will be check by principal ");
					}
				} catch (TeacherNotFoundException e) {
					System.out.println(e.getMessage());
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
				
				
			case 5:
				System.out.println("Enter student Name ");
				s.nextLine();
				String sname = s.nextLine();
				try {
					int result = teacherRef.grantLeave(sname);
					if (result == 1) {
						System.out.println("leave granted!! Thank you");
					}
				} catch (StudentNotFoundException e) {
					System.out.println(e.getMessage());
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			
			
			}

		}while(n!=0);
			
			
			
			
			
			
			
			
		case 3:
			int principal=0;
			do {
				System.out.println("------------------Principal function------------");
				System.out.println( "1. Add teacher \n"
						+ "2. Update Teacher \n"
						+ "3. Delete Teacher \n"
						+ "4.  show Teacher Lecture \n"
						+ "5. Update Teacher Lecture \n"
						+ "6. Check and Grant Leave \n"
						+ "7. Enter 0 to exit ");
				 principal=kb.nextInt();
				
				switch(principal)
				{
				
				case 1:
					try {
					System.out.println("enter teacher name");
					String name=kb.next();
					System.out.println("enter tid");
					int id=kb.nextInt();
					System.out.println("enter subject");
					String subject=kb.next();
					System.out.println("enter department");
					String dept= kb.next();
					Teacher teacher= new Teacher();
					teacher.setTeacherName(name);
					teacher.setTeacherId(id);
					teacher.setType("Teacher");
					teacher.setSubject(subject);
					teacher.setDepartment(dept);
					teacher.setLeave(0);
					int result=PrincipalServiceImpl.addTeacher(teacher);
					if(result!=0)
						System.out.println("teacher Added");
					System.out.println("press 11 to add lecture or 12 ");
					int addLecture=kb.nextInt();
					Lecture lecture = new Lecture();
					if(addLecture==11) {
						System.out.println("enter first lecture");
						kb.nextLine();
						String firstLec= kb.nextLine();
						
						System.out.println("enter second lecture");
						//kb.nextLine();
						String secondLec= kb.nextLine();
						
						System.out.println("enter third lecture");
						
						String thirdLec= kb.nextLine();
						lecture.setFirstLec(firstLec);
						lecture.setSecondLec(secondLec);
						lecture.setThirdLec(thirdLec);
						lecture.setTeacherId(id);
						lecture.setTeacherName(name);
						int addResult= PrincipalServiceImpl.insertLecture(lecture);
						if(addResult!=0) {
							System.out.println(" Added Lectures");
						}
						
						
					}
					else
						{
						lecture.setTeacherId(id);
						lecture.setTeacherName(name);
						PrincipalServiceImpl.insertLecture(lecture);
						System.out.println("Lecture set to free");
						}
					}catch(TeacherNotAddException e) {
						System.out.println(e.getMessage());
					}
				break;
				case 2:
					try {
						System.out.println("enter teacher name to update");
						kb.nextLine();
						String name= kb.nextLine();
						System.out.println("enter subject");
						String subject= kb.nextLine();
						System.out.println("enter department");
						String dept= kb.nextLine();
						Teacher teacher= new Teacher();
						teacher.setSubject(subject);
						teacher.setTeacherName(name);
						teacher.setDepartment(dept);
							int result= PrincipalServiceImpl.updateTeacherDetails(teacher, name);
							if(result!=0)
								System.out.println("teacher updated!! Thank you");
						}catch(Exception e) {
							System.out.println(e.getMessage());
						}
					break;
				case 3:
					try {
						System.out.println("enter teacher name to delete");
						kb.nextLine();
						String name= kb.nextLine();
							int result= PrincipalServiceImpl.deleteTeacher(name);
							if(result!=0)
								System.out.println("teacher delete successfully");
						}catch(Exception e) {
							System.out.println(e.getMessage());
						}
					break;
				case 4:
						try {
						System.out.println("enter name");
						kb.nextLine();
						String name= kb.nextLine();
						List<String> lectures= PrincipalServiceImpl.showTeacherLecture(name);
						for(int i=0;i<lectures.size();i++) {
							System.out.println((i+1)+" lectures "+lectures.get(i));
							}
						}
						catch(Exception e) {
							System.out.println(e.getMessage());
						}
					break;
				case 5:
						try {
						System.out.println("enter teacher name to update lecture");
						kb.nextLine();
						String name= kb.nextLine();
						List<String> lectures= PrincipalServiceImpl.showTeacherLecture(name);
						for(int i=0;i<lectures.size();i++) {
							System.out.println((i+1)+" lectures "+lectures.get(i));
							}
						System.out.println("enter lectureNumber you want to update");
						int lectureNumber=kb.nextInt();
						System.out.println("enter class name to update");
						kb.nextLine();
						String lectureName= kb.nextLine();
						int result= PrincipalServiceImpl.updateTeacherLecture(lectureNumber, lectureName, name);
						if(result!=0) {
							System.out.println("updated!! thank you..");
						}
						}catch(TeacherNotFoundException e) {
							System.out.println(e.getMessage());
						}
					break;
				case 6:
						try {
							boolean checkLeave=PrincipalServiceImpl.grantLeave();
							if(checkLeave) {
								System.out.println("leave Granted");
								}
								} catch (NoTeacherAvailableException e) {
									System.out.println(e.getMessage());
							
								} catch (Exception e) {
									System.out.println(e.getMessage());
							
								}
							break;
				
				
					
				}
				}while(principal!= 0);
			
	}
		}while(type!=404);
	}
}

