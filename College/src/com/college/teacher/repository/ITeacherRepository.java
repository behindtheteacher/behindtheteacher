package com.college.teacher.repository;

import java.util.List;
import com.college.teacher.model.Teacher;

public interface ITeacherRepository {
	
	Teacher searchByName(String name) throws Exception;

	List<Teacher> searchByDepartment(String dept) throws Exception;

	int applyLeave(String name) throws Exception;

	int grantLeave(String name) throws Exception;

	int updateLeave(String name) throws Exception;
}
