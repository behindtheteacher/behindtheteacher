package com.college.teacher.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.college.dao.ModelDao;
import com.college.exception.TeacherNotFoundException;
import com.college.teacher.model.Teacher;

public class TeacherRepositoryImpl implements ITeacherRepository {
	Connection connection = null;
	PreparedStatement statement = null;

	
	@Override
	public Teacher searchByName(String name) throws Exception {

		Teacher teacher = null;
		try {

			String namesql = "Select * from Teacher where teacherName=?";
			connection = ModelDao.openConnection();
			statement = connection.prepareStatement(namesql);
			statement.setString(1, name.toUpperCase());
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				teacher = new Teacher();
				teacher.setTeacherName(rs.getString(1));
				teacher.setTeacherId(rs.getInt(2));
				teacher.setType(rs.getString(3));
				teacher.setSubject(rs.getString(4));
				teacher.setDepartment(rs.getString(5));
				teacher.setLeave(rs.getInt(6));
				//teacher.setSection(rs.getString(7));
			}

		} catch (SQLException e) {

			e.printStackTrace();
			throw e;
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();

			}
			ModelDao.closeConnection();
		}

		return teacher;
	}

	@Override
	public List<Teacher> searchByDepartment(String dept) throws Exception {
		List<Teacher> teacherByDept = new ArrayList<>();
		try {
			String deptsql = "Select * from Teacher where department=?";
			connection = ModelDao.openConnection();
			statement = connection.prepareStatement(deptsql);
			statement.setString(1, dept.toUpperCase());
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				Teacher teacher = new Teacher();
				teacher.setTeacherName(rs.getString(1));
				teacher.setTeacherId(rs.getInt(2));
				teacher.setType(rs.getString(3));
				teacher.setSubject(rs.getString(4));
				teacher.setDepartment(rs.getString(5));
				teacher.setLeave(rs.getInt(6));
				//teacher.setSection(rs.getString(7));
				teacherByDept.add(teacher);

			}
		} catch (SQLException e) {

			e.printStackTrace();
			throw e;
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ModelDao.closeConnection();
		}

		return teacherByDept;
	}

	

	@Override
	public int applyLeave(String name) throws Exception {
		int result = 0;
		try {

			String updsql = "update Teacher set leaves=1 where teacherName=?";
			connection = ModelDao.openConnection();
			statement = connection.prepareStatement(updsql);
			statement.setString(1, name.toUpperCase());
			result = statement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			throw e;
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();

			}
			ModelDao.closeConnection();
		}

		return result;
	}

	@Override
	public int grantLeave(String name) throws Exception {
		int result = 0;
		try {

			String countsql = "select count(*) from student where leaves=1";
			connection = ModelDao.openConnection();
			statement = connection.prepareStatement(countsql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				result = rs.getInt(1);
			}

		} catch (SQLException e) {

			e.printStackTrace();
			throw e;
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();

			}
			ModelDao.closeConnection();
		}

		return result;
	}

	@Override
	public int updateLeave(String name) throws Exception {
		int result = 0;
		try {

			String updsql = "update student set leaves=1 where stdName=?";
			connection = ModelDao.openConnection();
			statement = connection.prepareStatement(updsql);
			statement.setString(1, name.toUpperCase());
			result = statement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			throw e;
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();

			}
			ModelDao.closeConnection();
		}

		return result;
	}

}
