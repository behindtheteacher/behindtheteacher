package com.college.teacher.service;



import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.college.exception.StudentNotFoundException;
import com.college.exception.TeacherNotFoundException;
import com.college.teacher.model.Teacher;
import com.college.teacher.repository.ITeacherRepository;
import com.college.teacher.repository.TeacherRepositoryImpl;

public class TeacherServiceImpl implements ITeacherService {
	ITeacherRepository teacherRepository = new TeacherRepositoryImpl();

	@Override
	public Teacher searchByName(String name) throws TeacherNotFoundException, Exception {
		Teacher teacherByName = teacherRepository.searchByName(name);
		if (teacherByName == null) {
			throw new TeacherNotFoundException("Name is not Found");
		}
		return teacherByName;
	}

	@Override
	public List<Teacher> searchByDepartment(String dept) throws TeacherNotFoundException, Exception {
		List<Teacher> teacherList = teacherRepository.searchByDepartment(dept).stream()
				.sorted(Comparator.comparing(Teacher::getTeacherName)).collect(Collectors.toList());

		if (teacherList.isEmpty())
			throw new TeacherNotFoundException("Enter Correct department ");
		return teacherList;
	}

	@Override
	public int applyLeave(String name) throws TeacherNotFoundException, Exception {
		int result = teacherRepository.applyLeave(name);
		if (result == 0)
			throw new TeacherNotFoundException("Please Enter the Correct Name");
		return result;
	}

	@Override
	public int grantLeave(String name) throws StudentNotFoundException, Exception {
		int result = teacherRepository.grantLeave(name);
		 if (result >= 3) {
			throw new StudentNotFoundException("Limit Reached Can't Apply Leave!");
		} else {
			int leave= teacherRepository.updateLeave(name);
			if(leave==0) {
				throw new StudentNotFoundException("pls enter correct names");
			}
		}
		return 1;
	}

	@Override
	public List<Teacher> showSchedule(String section) throws TeacherNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	

}
