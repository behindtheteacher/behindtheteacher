package com.college.teacher.service;

import java.util.List;

import com.college.exception.StudentNotFoundException;
import com.college.exception.TeacherNotFoundException;
import com.college.teacher.model.Teacher;

public interface ITeacherService {
	
	List<Teacher> showSchedule(String section) throws TeacherNotFoundException;

	Teacher searchByName(String name) throws TeacherNotFoundException, Exception;

	List<Teacher> searchByDepartment(String dept) throws TeacherNotFoundException, Exception;

	int applyLeave(String name) throws TeacherNotFoundException, Exception;

	int grantLeave(String name) throws StudentNotFoundException, Exception;

}
