package com.college.teacher.model;

public class Teacher {
	String teacherName;
	int teacherId;
	String type;
	String subject;
	String department;
	int leave;
	
public Teacher(String teacherName, int teacherId, String type, String subject, String department, int leave) {
		super();
		this.teacherName = teacherName;
		this.teacherId = teacherId;
		this.type = type;
		this.subject = subject;
		this.department = department;
		this.leave = leave;
	}
@Override
	public String toString() {
		return "Teacher [teacherName=" + teacherName + ", teacherId=" + teacherId + ", type=" + type + ", subject="
				+ subject + ", department=" + department + ", leave=" + leave + "]";
	}
public Teacher() {
		super();
	}

public String getTeacherName() {
	return teacherName;
}
public void setTeacherName(String teacherName) {
	this.teacherName = teacherName;
}
public int getTeacherId() {
	return teacherId;
}
public void setTeacherId(int teacherId) {
	this.teacherId = teacherId;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getSubject() {
	return subject;
}
public void setSubject(String subject) {
	this.subject = subject;
}
public String getDepartment() {
	return department;
}
public void setDepartment(String department) {
	this.department = department;
}
public int getLeave() {
	return leave;
}
public void setLeave(int leave) {
	this.leave = leave;
}


	
}
