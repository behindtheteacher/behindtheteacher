package com.college.lecture.model;

public class Lecture {
	public int getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(int teacherId) {
		this.teacherId = teacherId;
	}
	public String getFirstLec() {
		return firstLec;
	}
	public void setFirstLec(String firstLec) {
		this.firstLec = firstLec;
	}
	public String getSecondLec() {
		return secondLec;
	}
	public void setSecondLec(String secondLec) {
		this.secondLec = secondLec;
	}
	public String getThirdLec() {
		return thirdLec;
	}
	public void setThirdLec(String thirdLec) {
		this.thirdLec = thirdLec;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	@Override
	public String toString() {
		return "Lecture [teacherId=" + teacherId + ", firstLec=" + firstLec + ", secondLec=" + secondLec + ", thirdLec="
				+ thirdLec + ", teacherName=" + teacherName + "]";
	}
	public Lecture() {
		super();
	}
	public Lecture(int teacherId, String firstLec, String secondLec, String thirdLec, String teacherName) {
		super();
		this.teacherId = teacherId;
		this.firstLec = firstLec;
		this.secondLec = secondLec;
		this.thirdLec = thirdLec;
		this.teacherName = teacherName;
	}
	int teacherId;
	String firstLec;
	String secondLec;
	String thirdLec;
	String teacherName;

}
