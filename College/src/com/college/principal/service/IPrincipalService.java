package com.college.principal.service;

import java.util.List;

import com.college.exception.NoTeacherAvailableException;
import com.college.exception.TeacherNotAddException;
import com.college.exception.TeacherNotFoundException;
import com.college.lecture.model.Lecture;
import com.college.teacher.model.Teacher;

public interface IPrincipalService {
	public boolean grantLeave() throws NoTeacherAvailableException,Exception;
	public List<String> showTeacherLecture(String teacherName) throws TeacherNotFoundException, Exception;
	public int addTeacher(Teacher teacher) throws TeacherNotAddException,Exception;
	public int updateTeacherLecture(int lectureNumber, String lectureSection,String name) throws TeacherNotFoundException;
	public int deleteTeacher(String teacherName) throws TeacherNotAddException,Exception;
	public int updateTeacherDetails(Teacher teacher,String teacherName) throws TeacherNotAddException, Exception;
	public int insertLecture(Lecture lecture) throws Exception;



}
