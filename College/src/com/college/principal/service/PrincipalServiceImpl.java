package com.college.principal.service;


import java.util.List;

import com.college.exception.NoTeacherAvailableException;
import com.college.exception.TeacherNotAddException;
import com.college.exception.TeacherNotFoundException;
import com.college.lecture.model.Lecture;
import com.college.principal.repo.IPrincipalRepo;
import com.college.principal.repo.PrincipalRepo;
import com.college.teacher.model.Teacher;

public class PrincipalServiceImpl implements IPrincipalService{
	IPrincipalRepo principalRepo= new PrincipalRepo();
	@Override
	public boolean grantLeave() throws NoTeacherAvailableException, Exception {
		boolean checkLeaveGrant= principalRepo.grantLeave();
		if(checkLeaveGrant) {
			return true;
		}
		else
			throw new NoTeacherAvailableException("No teacher is free.. cannot grant leave");
		
	}

	@Override
	public List<String> showTeacherLecture(String teacherName) throws Exception {
		List<String> lectures= null;
		
			lectures = principalRepo.showTeacherLecture(teacherName);
			if(lectures.isEmpty()) {
				throw new TeacherNotFoundException("pls enter correct name");
			}
			else
				return lectures;
	}

	

	@Override
	public int addTeacher(Teacher teacher) throws TeacherNotAddException,Exception {
		int result= principalRepo.addTeacher(teacher); 
		if(result!=0)
			return result;
		else
			throw new TeacherNotAddException("cannot add teacher.. try again");
	}

	@Override
	public int updateTeacherLecture(int lectureNumber, String lectureSection, String name)
			throws TeacherNotFoundException {
		try {
			int result= principalRepo.updateTeacherLecture(lectureNumber, lectureSection, name);
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
		throw new TeacherNotFoundException("Teacher cannot be updated");
		}
	}

	@Override
	public int deleteTeacher(String teacherName) throws Exception {
		int result=0;	
		
				result= principalRepo.deleteTeacher(teacherName);
				if(result!=0) 
					return result;
				else
					throw new Exception("pls enter correct name!! cannt deleted");
			
	}

	@Override
	public int updateTeacherDetails(Teacher teacher, String teacherName) throws Exception {
		
			int result= principalRepo.updateTeacherDetails(teacher, teacherName);
			if(result!=0) 
				return result;
			else
				throw new Exception("Cannot Updated !! pls enter correct teacher NameS");
		}

	@Override
	public int insertLecture(Lecture lecture) throws Exception {
		int result= principalRepo.insertLecture(lecture);
		return result;
	}
		
	

}
