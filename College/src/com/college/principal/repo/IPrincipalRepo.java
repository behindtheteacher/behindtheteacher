package com.college.principal.repo;

import java.util.List;

import com.college.lecture.model.Lecture;
import com.college.teacher.model.Teacher;

public interface IPrincipalRepo {
	public boolean grantLeave() throws Exception;
	public List<String> showTeacherLecture(String teacherName) throws Exception;
	public int addTeacher(Teacher teacher) throws Exception;
	public int updateTeacherLecture(int lectureNumber, String lectureSection,String name) throws Exception;
	public int deleteTeacher(String teacherName) throws Exception;
	public int updateTeacherDetails(Teacher teacher,String teacherName) throws Exception;
	public int insertLecture(Lecture lecture) throws Exception;
}
