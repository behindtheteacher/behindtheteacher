package com.college.principal.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.college.dao.ModelDao;
import com.college.exception.NoTeacherAvailableException;
import com.college.exception.TeacherNotFoundException;
import com.college.lecture.model.Lecture;
import com.college.teacher.model.Teacher;

public class PrincipalRepo implements IPrincipalRepo{
	
	Connection connection=null;
	PreparedStatement preparedStmt= null;
	ResultSet resultSet= null;
	List<String> teacherName= new ArrayList<>();
	int totalColumn=3;
	
	
	
	@Override
	public boolean grantLeave() throws Exception {
		int value=0;
		int freeLectureCount=0;
		String sql="select teacherName from teacher where leaves='1'";
		connection= ModelDao.openConnection();
		try {
			preparedStmt= connection.prepareStatement(sql);
			resultSet= preparedStmt.executeQuery();
			while(resultSet.next()) {
					if(!teacherName.contains(resultSet.getString(1)))
						teacherName.add(resultSet.getString(1));
			}
			int engageCount= showToEngageLecture(teacherName);
			System.out.println(teacherName.size()+" Teacher applied for leave  " +teacherName);
			System.out.println("thier lecture to engage -- "+ engageCount );
			System.out.println();
			System.out.println("free Lectures of other Teachers");
			
			//HashMap<String,Integer> assign= checkFreeLectures();
			HashMap<String,Integer> assign= freeLec;
			//System.out.println(assign);
			Set<String> allName=assign.keySet();
			for(int i=0;i<allName.size();i++) {
				
				if(assign.containsKey(teacherName.get(i))) {
					assign.remove(teacherName.get(i));
				}
			
			}
			//System.out.println(assign);
			for (Map.Entry<String, Integer> set :
	             assign.entrySet()) {
				value= set.getValue();
				freeLectureCount= freeLectureCount+ value;
	            // Printing all elements of a Map
	            System.out.println("TeacherName -- "+set.getKey() +" No. of free Lectures -- "
	                               + set.getValue());
	        }
			
			if(freeLectureCount<engageCount)
				return false;
			else
				return true;
			
			//System.out.println(freeLectureCount)
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		} 
		finally {
			if(connection!= null)
				connection.close();
			if(preparedStmt!= null) {
				preparedStmt.close();
			}
		}
		
	}

	
	
	@Override
	public List<String> showTeacherLecture(String teacherName) throws Exception {
		List<String> lectures= new ArrayList<>();
		String sql="select firstLec,secondLec,thirdLec from lecture where teacherName=?";
		connection= ModelDao.openConnection();
		try {
			preparedStmt= connection.prepareStatement(sql);
			preparedStmt.setString(1, teacherName);
			resultSet= preparedStmt.executeQuery();
			while(resultSet.next()) {
				lectures.add(resultSet.getString(1));
				lectures.add(resultSet.getString(2));
				lectures.add(resultSet.getString(3));
				
			}
			return lectures;
		}catch(Exception e) {
			throw e;
		}
		finally {
			if(connection!= null)
				connection.close();
			if(preparedStmt!= null) {
				preparedStmt.close();
			}
		}
		
		
		
		
		
		
	}
	HashMap<String, Integer> freeLec= new HashMap<>();
	private HashMap<String,Integer> checkFreeLectures() throws Exception {
		
		String query="select * from lecture";
		connection= ModelDao.openConnection();
		try {
			preparedStmt= connection.prepareStatement(query);
			resultSet= preparedStmt.executeQuery();
			while(resultSet.next()) {
					
						int countLeave=0;
						int teacherId= resultSet.getInt(1);
						String firstLec= resultSet.getString(2);
						String secondLec= resultSet.getString(3);
						String thirdLec= resultSet.getString(4);
						String teacherName= resultSet.getString(5);
						if(firstLec.equalsIgnoreCase("Free") && secondLec.equalsIgnoreCase("Free") && thirdLec.equalsIgnoreCase("Free"))
							countLeave=countLeave+3;
						else if((firstLec.equalsIgnoreCase("Free") && secondLec.equalsIgnoreCase("Free")) || (secondLec.equalsIgnoreCase("Free") && thirdLec.equalsIgnoreCase("Free")) || (firstLec.equalsIgnoreCase("Free")&& thirdLec.equalsIgnoreCase("Free")))
							countLeave= countLeave+2;
						else if (firstLec.equalsIgnoreCase("Free") || secondLec.equalsIgnoreCase("Free") || thirdLec.equalsIgnoreCase("Free"))
							countLeave=countLeave+1;
						else
							countLeave=0;
						freeLec.put(teacherName, countLeave);
				}
				
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			if(connection!= null)
				connection.close();
			if(preparedStmt!= null) {
				preparedStmt.close();
			}
		}
		return freeLec;
	}

	
	private int showToEngageLecture(List<String > teacherNames) throws Exception {
		int count=0;
		HashMap<String,Integer> getAllDetails= checkFreeLectures();
		Set<String> allName=getAllDetails.keySet();
		for(int i=0;i<teacherNames.size();i++) {
				if(allName.contains(teacherName.get(i))) {
					int val= getAllDetails.get(teacherName.get(i));
					count=count+(totalColumn-val);
				}
			}
		return count;
		
	}
	
	
	
	



	



	@Override
	public int addTeacher(Teacher teacher) throws Exception {
		try {
		String sql="insert into teacher values(?,?,?,?,?,?)";
		connection= ModelDao.openConnection();
		preparedStmt= connection.prepareStatement(sql);
		preparedStmt.setString(1, teacher.getTeacherName());
		preparedStmt.setInt(2, teacher.getTeacherId());
		preparedStmt.setString(3, teacher.getType());
		preparedStmt.setString(4, teacher.getSubject());
		preparedStmt.setString(5, teacher.getDepartment());
		preparedStmt.setInt(6, teacher.getLeave());
		int result= preparedStmt.executeUpdate();
		return result;
		}
		catch(Exception e) {
			throw e;
		}
		finally {
			if(connection!= null)
				connection.close();
			if(preparedStmt!= null) {
				preparedStmt.close();
			}
		}
		
	}



	@Override
	public int updateTeacherLecture(int lectureNumber, String lectureSection,String name) throws Exception {
		// TODO Auto-generated method stub
		try {
		String lectureColumn="";
		if(lectureNumber==1) {
			lectureColumn="firstLec";
		}
		else if(lectureNumber==2) {
			lectureColumn="secondLec";
		}
		else if(lectureNumber==3) {
			lectureColumn="thirdLec";
		}
		
		String sql="update lecture set "+lectureColumn+ "='"+ lectureSection+"' where teacherName= ?" ;
		connection= ModelDao.openConnection();
		preparedStmt= connection.prepareStatement(sql);
		preparedStmt.setString(1, name);
		System.out.println(sql);
		int result= preparedStmt.executeUpdate();
		return result;
		}
		catch(Exception e) {
			throw e;
		}
		finally {
			if(connection!= null)
				connection.close();
			if(preparedStmt!= null) {
				preparedStmt.close();
			}
		}
	}



	@Override
	public int deleteTeacher(String teacherName) throws Exception {
		try {
		String sql="delete from teacher where teacherName=?";
		connection= ModelDao.openConnection();
		preparedStmt= connection.prepareStatement(sql);
		preparedStmt.setString(1, teacherName);
		int result= preparedStmt.executeUpdate();
		return result;
		}catch(Exception e) {
			throw e;
		}
		finally {
			if(connection!= null)
				connection.close();
			if(preparedStmt!= null) {
				preparedStmt.close();
			}
		}
	}



	@Override
	public int updateTeacherDetails(Teacher teacher,String teacherName) throws Exception {
		try {
		String sql="update teacher set teacherName=?,subject=?,department=? where teacherName=?";
		connection= ModelDao.openConnection();
		preparedStmt= connection.prepareStatement(sql);
		preparedStmt.setString(1, teacher.getTeacherName());
		preparedStmt.setString(2, teacher.getSubject());
		preparedStmt.setString(3, teacher.getDepartment());
		preparedStmt.setString(4, teacher.getTeacherName());
		int result= preparedStmt.executeUpdate();
		return result;
		}catch(Exception e) {
			throw e;
		}
		finally {
			if(connection!= null)
				connection.close();
			if(preparedStmt!= null) {
				preparedStmt.close();
			}
		}
	}
	
	public int getTeacherId(String teacherName) throws SQLException {
		int teacherId=0;
		String sqlTeachId="select teacherId from teacher where teacherName=?";
		connection= ModelDao.openConnection();
		preparedStmt= connection.prepareStatement(sqlTeachId);
		preparedStmt.setString(1, teacherName);
		resultSet= preparedStmt.executeQuery();
		while(resultSet.next()) {
		 teacherId= resultSet.getInt(1);
		}
		return teacherId;
	}
	
	public int insertLecture(Lecture lecture) throws Exception {
		//String sqlTeachId="select teacherId from teacher where teacherName=?";
		String sql="insert into lecture values (?,?,?,?,?)";
		//int teacherId= getTeacherId(lecture.getTeacherName());
		///System.out.println(teacherId);
		connection= ModelDao.openConnection();
		preparedStmt= connection.prepareStatement(sql);
		preparedStmt.setInt(1, lecture.getTeacherId());
		preparedStmt.setString(5, lecture.getTeacherName());
		if(lecture.getFirstLec()== null && lecture.getSecondLec()== null && lecture.getThirdLec()== null)
		{
			preparedStmt.setString(2, "Free");
			preparedStmt.setString(3, "Free");
			preparedStmt.setString(4, "Free");
			
		}
		else
			{preparedStmt.setString(2, lecture.getFirstLec());
			preparedStmt.setString(3, lecture.getSecondLec());
			preparedStmt.setString(4, lecture.getThirdLec());
			}
			int result= preparedStmt.executeUpdate();
		
		return result;
	}

}
